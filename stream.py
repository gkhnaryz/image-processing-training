import numpy as np
import cv2

cap = cv2.VideoCapture('1.mp4')  # Video dosyasını aç

fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows=True)  # background substractor olustur

while (cap.isOpened()):
    ret, frame = cap.read()  # frame yakala

    fgmask = fgbg.apply(frame)  # substractor uygula

    try:
        cv2.imshow('Frame', frame)
        cv2.imshow('Background Substraction', fgmask)
    except:
        # eğer gösterilecek bir frame yok ise fırlat
        print('EOF')
        break

    #  kapat 'Q' or ESC
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()  # release video file
cv2.destroyAllWindows()  # close all openCV windows